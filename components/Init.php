<?php

namespace app\components;

use Yii;
use yii\helpers\Url;

class Init  extends \yii\base\Component
{
	public function init() {
		if( Yii::$app->getUser()->isGuest && Yii::$app->getRequest()->url !== Url::to('/site/login') ) {
			Yii::$app->getResponse()->redirect('/site/login');
		}

		parent::init();
	}
}