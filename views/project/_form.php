<?php

use yii\helpers\Html;
use app\models\User;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $users = User::find()->asArray()->all(); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), 
        [
            'data' => ArrayHelper::map($users, 'id', 'fio'),
            'language' => 'ru',
    ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <?= $form->field($model, 'start_at')->widget(DatePicker::classname(), [
      'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
      ]
    ]); ?>

    <?= $form->field($model, 'finish_at')->widget(DatePicker::classname(), [
      'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
      ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
