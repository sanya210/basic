<?php

use yii\db\Migration;

/**
 * Class m180903_164029_project
 */
class m180903_164029_project extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'cost' => $this->integer(),
            'start_at' => $this->date(),
            'finish_at' => $this->date(),
        ], $tableOptions); 
    
        $this->addForeignKey(
            'FK_project_has_user', '{{%project}}', 'user_id', '{{%user}}', 'id', 'CASCADE'
        );
    }


    public function down()
    {
        $this->dropTable('{{%project}}');
        
        return true;
    }
}
